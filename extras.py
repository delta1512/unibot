# -*- coding: utf-8 -*-

import aiohttp
import io
import re
import json
import math
import random as r
from math import ceil
from time import time, sleep
from statistics import mean
import discord
import requests

import queries as q
import symbols as sym
from b_string import BString


UNI_START = 1595188800
UNI_END = 1603450800

LOCAL_WEATHER = 'http://api.openweathermap.org/data/2.5/forecast?id=2153471&appid=e93ba752a02a266608e870e757dee863&units=metric'
ICON_URL = 'http://openweathermap.org/img/w/{}.png'

WEEK = 7 * 24 * 60 * 60
MONTH = 30 * 24 * 60 * 60
YEAR = 365 * 24 * 60 * 60
DAY = 24 * 60 * 60
HOUR = 60 * 60
MIN = 60

DAYS = (
    'monday',
    'tuesday',
    'wednesday',
    'thursday',
    'friday',
    'saturday',
    'sunday'
)

PRIMES = (2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229)

IS_QUESTIONS = (
    'mod',
    'big mod',
    'mult inv',
    'tot',
    'gcd'
)


async def get_weather():
    async with aiohttp.ClientSession() as session:
        async with session.get(LOCAL_WEATHER) as resp:
            raw = await resp.read()
            text = raw.decode('utf-8', 'ignore')
            response = json.loads(text)

        worst_weather = 'No data'
        worst_weather_id = 999
        worst_weather_icon = ''
        temp_series = []
        humidity_series = []
        for i in range(7):
            hour_data = response['list'][i]
            temp_series.append(hour_data['main']['temp'])
            humidity_series.append(hour_data['main']['humidity'])
            current_weather = hour_data['weather'][0]['id']
            if current_weather < worst_weather_id:
                worst_weather = hour_data['weather'][0]['main']
                worst_weather_id = current_weather
                worst_weather_icon = hour_data['weather'][0]['icon']
        max_temp = max(temp_series)
        min_temp = min(temp_series)
        avg_humidity = round(mean(humidity_series), 2)
        return {
            'weather'   :   worst_weather,
            'min_temp'  :   min_temp,
            'max_temp'  :   max_temp,
            'humidity'  :   avg_humidity,
            'icon_url'  :   ICON_URL.format(worst_weather_icon)
        }


def get_roll(n):
    return r.randint(1, n)


def get_offset(time_string):
    total_offset = 0
    prev = 0
    for match in list(re.finditer('[aA-zZ]', time_string)):
        i = match.start()
        try:
            val = int(time_string[prev:i])
        except:
            return None
        denom = time_string[i]
        if denom == 'y':
            val = val * YEAR
        elif denom == 'M':
            val = val * MONTH
        elif denom == 'w':
            val = val * WEEK
        elif denom == 'd':
            val = val * DAY
        elif denom == 'h':
            val = val * HOUR
        elif denom == 'm':
            val = val * MIN
        total_offset += val
        prev = i + 1
    return total_offset


def get_week():
    return ceil((time()-UNI_START)/WEEK)


def get_time_left():
    return round((UNI_END-time())/DAY, 1)


def format_weather(data):
    return f'''
Penrith will be {data["weather"]} with a top of {data["max_temp"]}°C and low of {data["min_temp"]}°C.
Humidity {data["humidity"]}%\n
'''


async def parse_notif(notif):
    notif = notif.replace('$weather', format_weather(await get_weather()))
    notif = notif.replace('$week', str(get_week()))
    notif = notif.replace('$daysleft', str(get_time_left()))
    return notif


async def make_reminder(ctx, t, reminder):
    if type(t) is str:
        t = get_offset(t) + time()
    if t is None:
        return 'Invalid time'
    try:
        await q.new_reminder(ctx.author.id, reminder, ctx.channel.id, int(t))
        return ':white_check_mark: | Reminder successfully made'
    except:
        return 'Failed to make reminder'


async def make_notif(channel, title):
    try:
        nid = await q.new_notification(channel, title)
        return f':white_check_mark: | Successfully made notification with notification ID `{nid}`'
    except:
        return 'Failed to make notification'


async def edit_notif(nid, attribute, value):
    try:
        exit_code = await q.edit_notification(nid, attribute, value)
        if exit_code:
            return ':white_check_mark: | Successfully edited notification'
    except:
        pass
    return 'Failed to edit notification'


async def make_class(uid, name):
    try:
        cid = await q.new_class(uid, name)
        return f':white_check_mark: | Successfully made class with class ID `{cid}`'
    except:
        return 'Failed to make class'


async def edit_class(uid, cid, attribute, value):
    try:
        exit_code = await q.edit_class(uid, cid, attribute, value)
        if exit_code:
            return ':white_check_mark: | Successfully edited class'
    except:
        pass
    return 'Failed to edit class'


async def get_classes(uid):
    classes = await q.get_current_classes(uid)
    final_msg = discord.Embed(colour=discord.Colour.red())
    if len(classes) == 0:
        final_msg.add_field(name='No classes for today')
        return final_msg
    for Class in classes:
        final_msg.add_field(name=f'{Class["name"]} ({Class["cid"]})',
        value=f'''
        {Class["time"]} in room {Class["room"]}
        Teacher: {Class["teacher"]}
        ''')
    return final_msg


def get_random_el():
    rand = r.random()
    if rand < 0.01:
        return sym.empty_set
    return r.randint(-10, 10)
    #return r.choice(sym.alphabet)


def get_random_set():
    rset = set()
    if r.random() < 0.005:
        return rset
    while (r.random() < 0.8):
        rset.add(get_random_el())
        if len(rset) > 8:
            break
    return rset


def compute(op, set1, set2):
    if op == sym.union:
        return set1.union(set2)
    if op == sym.intersect:
        return set1.intersection(set2)
    if op == sym.complement:
        return set1 - set2
    if op == sym.sym_diff:
        return set1.symmetric_difference(set2)


def get_set_question():
    clean = lambda x: str(x).replace("'", "").replace('set()', '{}')
    set1 = get_random_set()
    set2 = get_random_set()
    op1 = r.choice(sym.operators)
    if r.random() < 0.1:
        op2 = r.choice(sym.operators)
        set3 = get_random_set()
        answer = compute(op2, compute(op1, set1, set2), set3)
        question = f'({clean(set1)} {op1} {clean(set2)}) {op2} {clean(set3)}'
    else:
        answer = compute(op1, set1, set2)
        question = f'{clean(set1)} {op1} {clean(set2)}'
    return question, answer


def get_info_sec_question():
    q = r.choice(IS_QUESTIONS)

    if q == 'mod':
        a, m = r.randint(1, 500), r.randint(2, 100)
        question = f'What is {a} mod {m}?'
        answer = a % m
    elif q == 'big mod':
        b, p, m = r.randint(2, 10), r.randint(1, 255), r.randint(2, 50)
        question = f'What is {b}^{p} mod {m}?'
        answer = fast_mod(b, p, m)
    elif q == 'mult inv':
        p = r.choice(PRIMES)
        q = r.choice(tuple(set(PRIMES[:5]) - {p}))
        phi_n = (p - 1) * (q - 1)
        e = r.choice(tuple(set(range(2, phi_n)) & set(PRIMES)))
        question = f'What is the multiplicative inverse of {e} with respect to mod {phi_n}?'
        answer = get_mult_inverse(phi_n, e)
    elif q == 'tot':
        n = r.randint(5, 50)
        question = f'What is phi({n})?'
        answer = len(totient(n))
    elif q == 'gcd':
        a, b = r.randint(2, 500), r.randint(2, 500)
        question = f'What is gcd({a}, {b})?'
        answer = gcd(a, b)
    return question, answer



async def poll_reminders(client):
    try:
        reminders = await q.get_current_reminders()
        for reminder in reminders:
            msg = discord.Embed(title='Reminder!', description=f'<@{reminder["uid"]}> Reminder: {reminder["reminder"]}', colour=discord.Colour.orange())
            await client.get_channel(reminder['channel']).send(embed=msg)
            sleep(0.5)
    except Exception as E:
        print('Poll reminders failed: ', E)


async def poll_notifications(client):
    try:
        notifs = await q.get_current_notifications()
        for notif in notifs:
            content = await parse_notif(notif['content'])
            msg = discord.Embed(title=f'{notif["title"]} ({notif["nid"]})', description=content, colour=discord.Colour.purple())
            await client.get_channel(notif['channel']).send(embed=msg)
    except Exception as E:
        print('Poll notifications failed: ', E)


def get_suffix(n):
    end_char = int(str(n)[-1:])
    if 3 < n < 21:
        return 'th'
    elif end_char == 0:
        return 'th'
    elif end_char == 1:
        return 'st'
    elif end_char == 2:
        return 'nd'
    else:
        return 'rd'


def fast_mod(base: int, exp: int, mod: int):
    '''
    Modular arithmetic using the square and multiply algorithm.
    '''
    bin_exp = BString(exp)
    bin_exp = bin_exp[bin_exp.index('1'):] # Align to MSB

    # Calculate the incrementing powers % mod
    powers = []
    curr_exp = 2
    end = 2 ** (len(bin_exp) - 1)

    powers.append(base % mod) # Fix off-by-one bug

    while curr_exp <= end:
        powers.append((powers[-1] ** 2) % mod)
        curr_exp += curr_exp

    # Multiply and sum the corresponding powers of 2 according to the binary
    result = 1
    for bit in bin_exp:
        curr_mod = powers.pop()
        if bit == '1':
            result *= curr_mod

    # Perform the final modulo
    return result % mod


def get_mult_inverse(a: int, b: int):
    '''
    Extended GCD function to find a multiplicative inverse of b with respect to
    the modulo a.
    '''
    # To ensure correct output, swap the variables to be in order if b > a
    if b > a:
        tmp = a
        a = b
        b = tmp

    # a1 and b1 are omitted for efficiency reasons
    a2 = 0
    a3 = a
    b2 = 1
    b3 = b

    while b3 < 0 or b3 > 1:
        q = math.floor(a3 / b3)
        t2 = b2
        t3 = b3
        b2 = a2 - (b2 * q)
        b3 = a3 - (b3 * q)
        a2 = t2
        a3 = t3

    if b3 == 0:
        raise ValueError('{} does not have a multiplicative inverse under modulo {}'.format(b, a))
    else:
        # Make any negative values positive
        if b2 < 0:
            b2 = (b2 + a) % a
        return b2


def gcd(a: int, b: int):
    '''
    Finds the greatest common divisor of a and b.
    '''
    # To ensure correct output, swap the variables to be in order if b > a
    if b > a:
        tmp = a
        a = b
        b = tmp

    while b != 0:
        r = a % b
        a = b
        b = r

    return a


def totient(n: int):
    final = set()

    for i in range(1, n):
        if gcd(n, i) == 1:
            final.add(i)

    return final
