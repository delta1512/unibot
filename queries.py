import aiomysql
import time

import auth as a

### CONSTANTS

DEF_NOTIF = [
    None,       #nid
    0,          #channel
    0,          #next_time
    '',         #title
    '',         #content
    1,          #daily
    0,          #weekly
    0           #monthly
]

DEF_CLASS = [
    0,          #uid
    None,       #cid
    '',         #name
    '',         #room
    'John S.',  #teacher
    'Mon',      #day
    '00:00'     #time
]


###


async def is_user(uid):
    db = await aiomysql.connect(host=a.sql_db_host, user=a.sql_db_usr, password=a.sql_db_pass)
    c = await db.cursor()
    await c.execute(f'SELECT uid FROM unibot.udb WHERE uid=%s', (uid))
    response = await c.fetchall()
    db.close()
    return len(response) > 0


async def new_user(uid):
    db = await aiomysql.connect(host=a.sql_db_host, user=a.sql_db_usr, password=a.sql_db_pass)
    c = await db.cursor()
    await c.execute('INSERT INTO unibot.udb VALUES (%s);', (uid))
    await db.commit()
    db.close()


async def new_reminder(uid, reminder, channel, Time):
    db = await aiomysql.connect(host=a.sql_db_host, user=a.sql_db_usr, password=a.sql_db_pass)
    c = await db.cursor()
    await c.execute('INSERT INTO unibot.reminders VALUES (%s, %s, %s, %s);', (uid, reminder, channel, Time))
    await db.commit()
    db.close()


async def get_current_reminders():
    db = await aiomysql.connect(host=a.sql_db_host, user=a.sql_db_usr, password=a.sql_db_pass)
    c = await db.cursor(aiomysql.DictCursor)
    await c.execute('SELECT uid, channel, reminder FROM unibot.reminders WHERE time <= %s;', (round(time.time())))
    results = await c.fetchall()
    for result in results:
        await c.execute('DELETE FROM unibot.reminders WHERE uid=%s AND reminder=%s', (result['uid'], result['reminder']))
    await db.commit()
    db.close()
    return results


async def new_notification(channel, title):
    db = await aiomysql.connect(host=a.sql_db_host, user=a.sql_db_usr, password=a.sql_db_pass)
    c = await db.cursor()
    new_entry = DEF_NOTIF
    new_entry[1] = channel
    new_entry[3] = title
    await c.execute('INSERT INTO unibot.notifications VALUES (%s, %s, %s, %s, %s, %s, %s, %s);', tuple(new_entry))
    await db.commit()
    await c.execute('SELECT max(nid) FROM unibot.notifications;')
    nid = await c.fetchone()
    db.close()
    return nid[0]


async def new_class(uid, name):
    db = await aiomysql.connect(host=a.sql_db_host, user=a.sql_db_usr, password=a.sql_db_pass)
    c = await db.cursor()
    new_entry = DEF_CLASS
    new_entry[0] = uid
    new_entry[2] = name
    await c.execute('INSERT INTO unibot.classes VALUES (%s, %s, %s, %s, %s, %s, %s);', tuple(new_entry))
    await db.commit()
    await c.execute('SELECT max(cid) FROM unibot.classes;')
    cid = await c.fetchone()
    db.close()
    return cid[0]


async def get_current_classes(uid):
    db = await aiomysql.connect(host=a.sql_db_host, user=a.sql_db_usr, password=a.sql_db_pass)
    c = await db.cursor(aiomysql.DictCursor)
    await c.execute('SELECT * FROM unibot.classes WHERE day=%s AND uid=%s;', (time.strftime('%a').lower(), uid))
    results = await c.fetchall()
    db.close()
    return results


async def edit_notification(nid, attribute, value):
    nid = int(nid)
    if attribute in ['daily', 'weekly', 'monthly', 'next_time']:
        value = int(value)
    db = await aiomysql.connect(host=a.sql_db_host, user=a.sql_db_usr, password=a.sql_db_pass)
    c = await db.cursor(aiomysql.DictCursor)
    await c.execute('SELECT * FROM unibot.notifications WHERE nid=%s;', (nid))
    result = await c.fetchall()
    if len(result) > 0:
        result = result[0]
        if attribute in result:
            await c.execute(f'UPDATE unibot.notifications SET {attribute}=%s WHERE nid=%s;', (value, nid))
            await db.commit()
            db.close()
            return True
    db.close()
    return False


async def edit_class(uid, cid, attribute, value):
    db = await aiomysql.connect(host=a.sql_db_host, user=a.sql_db_usr, password=a.sql_db_pass)
    c = await db.cursor(aiomysql.DictCursor)
    await c.execute('SELECT * FROM unibot.classes WHERE cid=%s AND uid=%s;', (cid, uid))
    result = await c.fetchall()
    if len(result) > 0:
        result = result[0]
        if attribute in result:
            await c.execute(f'UPDATE unibot.classes SET {attribute}=%s WHERE cid=%s;', (value, cid))
            await db.commit()
            db.close()
            return True
    db.close()
    return False


async def get_current_notifications():
    db = await aiomysql.connect(host=a.sql_db_host, user=a.sql_db_usr, password=a.sql_db_pass)
    c = await db.cursor(aiomysql.DictCursor)
    await c.execute('SELECT * FROM unibot.notifications WHERE next_time <= %s;', (round(time.time())))
    results = await c.fetchall()
    for result in results:
        if result['monthly']:
            offset = 30 * 24 * 60 * 60
        elif result['weekly']:
            offset = 7 * 24 * 60 * 60
        else:
            offset = 24 * 60 * 60
        next_time = int(time.time() + offset)
        await c.execute('UPDATE unibot.notifications SET next_time=%s WHERE nid=%s', (next_time, result['nid']))
    await db.commit()
    db.close()
    return results
