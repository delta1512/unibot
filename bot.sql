CREATE SCHEMA IF NOT EXISTS unibot;

USE unibot;


CREATE TABLE IF NOT EXISTS udb (
  uid BIGINT UNSIGNED,

  PRIMARY KEY (uid)
);


CREATE TABLE IF NOT EXISTS reminders (
  uid BIGINT UNSIGNED,
  reminder VARCHAR(200),
  channel BIGINT UNSIGNED,
  time BIGINT UNSIGNED,

  PRIMARY KEY (uid, reminder)
);


CREATE TABLE IF NOT EXISTS notifications (
  nid INTEGER AUTO_INCREMENT,
  channel BIGINT UNSIGNED,
  next_time BIGINT UNSIGNED,
  title TINYTEXT,
  content TEXT,
  daily BOOLEAN,
  weekly BOOLEAN,
  monthly BOOLEAN,

  PRIMARY KEY (nid)
);

CREATE TABLE IF NOT EXISTS classes (
  uid BIGINT UNSIGNED,
  cid INTEGER AUTO_INCREMENT,
  name TINYTEXT,
  room TINYTEXT,
  teacher TINYTEXT,
  day TINYTEXT,
  time TINYTEXT,

  PRIMARY KEY (cid)
);
