# -*- coding: utf-8 -*-

empty_set = '∅'

union = '∪'

intersect = '∩'

complement = '-'

sym_diff = 'Δ'

operators = (union, intersect, complement, sym_diff)

alphabet = ('a', 'b', 'c', 'd', 'x', 'y', 'z', 't')
