import asyncio
import discord
from discord.ext import commands
from ast import literal_eval
from os import popen
from time import time
import subprocess as sp

import extras
import queries as q
import symbols as sym


PRE = ';'
current_answer = set()
client = commands.Bot(command_prefix=PRE)
latest_users = {}


@client.event
async def on_ready():
    if hasattr(client, 'initialised'):
        return  # Prevents multiple on_ready calls
    client.initialised = True
    while True:
        await extras.poll_reminders(client)
        await extras.poll_notifications(client)
        await asyncio.sleep(60)


@client.command()
async def unix(ctx):
    await ctx.send(':clock3: | {}'.format(round(time())))


@client.command()
async def random(ctx, bytes : int):
    if bytes > 1000:
        bytes = 1000
    with open('/dev/urandom', 'rb') as devrand:
        await ctx.send(devrand.read(bytes).decode('unicode_escape'))


@client.command()
async def dice(ctx, n : int):
    await ctx.send(f':game_die: | {extras.get_roll(n)}')


@client.command(aliases=['time'])
async def _time(ctx):
    await ctx.send(':clock3: | Showing timezone info:\n\tCanada Pacific :flag_ca: ' + popen('TZ="Canada/Pacific" date \'+%l:%M %p %a %b %d\'').read() + '\n\tCanada Mountain :flag_ca: ' + popen('TZ="Canada/Mountain" date \'+%l:%M %p %a %b %d\'').read() + '\n\tAmerica NY :flag_us: ' + popen('TZ="America/New_York" date \'+%l:%M %p %a %b %d\'').read() + '\n\tAustralia Sydney :flag_au: ' + popen('TZ="Australia/Sydney" date \'+%l:%M %p %a %b %d\'').read())


@client.command(aliases=['reminder'])
async def remind(ctx, Time):
    reminder = ctx.message.content.replace(f'{PRE}remind {Time} ', '')
    await ctx.send(await extras.make_reminder(ctx, Time, reminder))


@client.command()
async def newclass(ctx):
    name = ctx.message.content.replace(f'{PRE}newclass ', '')
    await ctx.send(await extras.make_class(ctx.author.id, name))


@client.command()
async def newnotif(ctx):
    title = ctx.message.content.replace(f'{PRE}newnotif ', '')
    await ctx.send(await extras.make_notif(ctx.channel.id, title))


@client.command()
async def eclass(ctx, cid, attribute):
    value = ctx.message.content.replace(f'{PRE}eclass {cid} {attribute} ', '')
    await ctx.send(await extras.edit_class(ctx.author.id, cid, attribute, value))


@client.command()
async def enotif(ctx, nid, attribute):
    value = ctx.message.content.replace(f'{PRE}enotif {nid} {attribute} ', '')
    await ctx.send(await extras.edit_notif(nid, attribute, value))


@client.command()
async def classes(ctx):
    await ctx.send(embed=await extras.get_classes(ctx.author.id))


@client.command()
async def week(ctx):
    await ctx.send(f':calendar: | It is week {extras.get_week()}')


@client.command(aliases=['set', 'dm', 'q', 'question'])
async def set_practice(ctx):
    global current_answer
    current_question = None
    while current_question is None:
        try:
            current_question, current_answer = extras.get_info_sec_question()
        except (AssertionError, ValueError):
            continue
    await ctx.send(f'`{current_question}`')


@client.command(aliases=['a'])
async def answer(ctx):
    global current_answer
    user_answer = ctx.message.content.replace(f'{PRE}a ', '').replace(f'{PRE}answer ', '')
    for char in sym.alphabet:
        user_answer.replace(char, f"'{char}'")
    #print(user_answer)
    #print(current_answer)
    try:
        user_answer = int(user_answer)
        #print('eval: ', user_answer)
        assert user_answer == current_answer
        current_answer = None
        await ctx.send(':white_check_mark:')
    except:
        await ctx.send(':x: Incorrect')


@client.command()
async def start(ctx, t: int):
    if extras.UNI_END < time():
        extras.UNI_START = t
        await ctx.send(':white_check_mark:')
    else:
        await ctx.send(':x:')


@client.command()
async def end(ctx, t: int):
    if extras.UNI_END < time():
        extras.UNI_END = t
        await ctx.send(':white_check_mark:')
    else:
        await ctx.send(':x:')


@client.command(aliases=['killme', 'howlong', 'f'])
async def e(ctx):
    await ctx.send(f':calendar: | This semester will end in {extras.get_time_left()} days')


@client.command()
async def feedback(ctx):
    reminder = ctx.message.content.replace(f'{PRE}feedback ', '')
    await ctx.send(await extras.make_reminder(ctx, extras.UNI_END, reminder))



def checkspam(user):
    global latest_users
    ctime = time()
    if user in latest_users:
        if ctime - latest_users[user] > 1:
            latest_users[user] = ctime
            return False
        else:
            latest_users[user] = ctime
            return True
    else:
        latest_users[user] = ctime
        return False


@client.event
async def on_message(msg):
    user = msg.author.id
    if checkspam(user):
        return
    if not await q.is_user(msg.author.id):
        await q.new_user(msg.author.id)
    await client.process_commands(msg)


try:
    with open('API.key', 'r') as key_file:
        API_KEY = str(key_file.read().replace('\n', ''))
except Exception:
    exit(1)

client.run(API_KEY)
